if [ -f "/home/$USER/.vimrc" ]; then
    echo "Vimrc already exists, backing up to .vimrc.old"
    mv /home/$USER/.vimrc /home/$USER/.vimrc.old
fi;
if [ -f "/home/$USER/.screenrc" ]; then
    echo "Screenrc already exists, backing up to .screenrc.old"
    mv /home/$USER/.screenrc /home/$USER/.screenrc.old
fi;
if [ -f "/home/$USER/.bash_aliases" ]; then
    echo "bash_aliases already exists, backing up to .bash_aliases.old"
    mv /home/$USER/.bash_aliases /home/$USER/.bash_aliases.old
fi;
echo "Copying vimrc and screenrc...";
cp .screenrc .vimrc .bash_aliases /home/$USER

echo "Installing vundle";
if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
    git clone git@github.com:gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim;
    echo "Done";
else
    echo "Vundle is already installed";
fi;

"reload Vim sensible
runtime! plugin/sensible.vim
"Make VIM modern only!
set nocompatible

"Let their be line numbers!
set number

"Syntax!
syntax on
"Move the horrible esc key to jj imap jj <Esc>
imap jj <Esc>
"Auto reload .vimrc after change!
"autocmd! bufwritepost .vimrc source %

"Auto Filetype detection (for syntax highlighting etc)
filetype on

"Vundle
set rtp+=~/.vim/bundle/Vundle.vim
let g:vundle_default_git_proto = "ssh"

"Custom mapleader
let mapleader=","

filetype plugin indent on
call vundle#begin()
"Let Vundle Manage Vundle
Bundle 'git@github.com:gmarik/vundle'
Bundle 'git@github.com:SirVer/ultisnips'
"Bundle 'git@github.com:Shougo/neosnippet'
Bundle 'git@github.com:fatih/vim-go'
"Bundle 'git@github.com:honza/vim-snippets'
"Plugin 'git@github.com:nsf/gocode', {'rtp': 'nvim/'}
Bundle 'git@github.com:itchyny/lightline.vim'
Bundle 'git@github.com:wannesm/wmnusmv.vim'

"Bundles
Bundle 'git@github.com:ctrlpvim/ctrlp.vim'
Bundle 'git@github.com:dkprice/vim-easygrep'
"Bundle 'scrooloose/syntastic'
Bundle 'git@github.com:Shougo/neocomplete.vim'
"Bundle 'Valloric/YouCompleteMe'
Bundle 'git@github.com:tpope/vim-surround'
Bundle 'git@github.com:mattn/emmet-vim'
"Bundle 'tomasr/molokai'
"Bundle 'sentientmachine/erics_vim_syntax_and_color_highlighting'
"Bundle 'altercation/vim-colors-solarized'
Bundle 'git@github.com:godlygeek/tabular'
Bundle 'git@github.com:vim-scripts/SQLUtilities'
Bundle 'git@github.com:vim-scripts/Align'
"Bundle 'git@github.com:honza/vim-snippets'
Bundle 'git@github.com:flazz/vim-colorschemes'
"Bundle 'git@github.com:vim-scripts/taglist.vim'
Bundle 'git@github.com:ervandew/supertab'
Bundle 'git@github.com:tpope/vim-sensible'
Bundle 'git@github.com:vim-scripts/a.vim'
"Bundle 'git@github.com:bling/vim-airline'
"Bundle 'git@github.com:xolox/vim-easytags'
"Bundle 'git@github.com:xolox/vim-misc'
Bundle 'git@github.com:scrooloose/nerdtree'
Bundle 'git@github.com:majutsushi/tagbar'
Bundle 'git@github.com:tpope/vim-fugitive'
"Bundle 'git@github.com:exu/pgsql.vim'
Bundle 'git@github.com:spacetekk/pgsql.vim'
call vundle#end()

"Ctrlp mapping
let g:ctrlp_map = '<c-p>'
let g:ctrp_cmd = 'CtrlPMixed'

let g:solarized_termcolors=16
let g:solarized_termtrans = 1
"Terminal colour pallete
set t_Co=16

"Molokai Setup
syntax enable
set formatoptions+=t
"set wrap
"Set a textwrapping width
"set tw=80

"Auto indenting
set autoindent
set smartindent

"Copy indentation
set copyindent

"Tab
set shiftwidth=4

"Show search matches
set showmatch

"Ignore case for search
set ignorecase

"Auto detect case for search
set smartcase

"Highlight search results
set hlsearch

"Modify terminal titlebar
set title

"Get rid of annoying backup files (never use em' anyway)
set nobackup
set noswapfile

"Neo complete setup:
set wildignore+=*.class

" Tab spacing
set ts=4
set sw=4

set tabstop=4
set shiftwidth=4
set expandtab

nnoremap <silent> <Leader>tl :TlistToggle<CR>
map <F7> mzgg=G`z<CR>

map <,f> *``

"Tag List setup
nnoremap <silent> <F8> :TagbarToggle<CR>

" Split navigation
" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-l> :wincmd l<CR>
nmap <silent> <c-h> :wincmd h<CR>

" Easytags
let g:easytags_always_enabled = 1
let g:easytags_async = 1

map <C-n> :NERDTreeToggle<CR>

" Default Neocomplete
"Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
            \ 'default' : '',
            \ 'vimshell' : $HOME.'/.vimshell_hist',
            \ 'scheme' : $HOME.'/.gosh_completions'
            \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
    return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
    " For no inserting <CR> key.
    "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
"inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"



" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
    let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'

" Run a bash script if it's available

nnoremap <leader>r :!bash run.sh<CR>
nnoremap <leader>t :GoTest<CR>

nnoremap <leader>tl :set background=light<CR>
nnoremap <leader>td :set background=dark<CR>

" Colorscheme
set background=dark
colorscheme solarized

"nnoremap <leader>sf        <Plug>SQLU_Formatter<CR>
"
" Better vmap than default.
vmap sf :SQLUFormatter<CR>
" Nice little undocumented feature. Disabling this will allow SQL Utilities to
" work within strings
let g:sqlutil_use_syntax_support = 0

nnoremap <leader>gi :GoImports<CR>
nnoremap <leader>gt :GoTest<CR>
au FileType go nmap <Leader>e <Plug>(go-rename)

let g:airline_powerline_fonts = 1
let g:airline#extensions#whitespace#mixed_indent_algo = 1

"let g:airline_left_sep=''
"let g:airline_right_sep=''

" Clear trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e


"hi StatusLine cterm=none ctermbg=none

" Default lightline
let g:lightline = {
            \ 'colorscheme': 'solarized',
            \ 'mode_map': { 'c': 'NORMAL' },
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'filename' ] ]
            \ },
            \ 'component_function': {
            \   'modified': 'LightLineModified',
            \   'readonly': 'LightLineReadonly',
            \   'fugitive': 'LightLineFugitive',
            \   'filename': 'LightLineFilename',
            \   'fileformat': 'LightLineFileformat',
            \   'filetype': 'LightLineFiletype',
            \   'fileencoding': 'LightLineFileencoding',
            \   'mode': 'LightLineMode',
            \ },
            \ }

function! LightLineModified()
    return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! LightLineReadonly()
    return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? '' : ''
endfunction

function! LightLineFilename()
    return ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
                \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
                \  &ft == 'unite' ? unite#get_status_string() :
                \  &ft == 'vimshell' ? vimshell#get_status_string() :
                \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
                \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction

function! LightLineFugitive()
    if &ft !~? 'vimfiler\|gundo' && exists("*fugitive#head")
        let _ = fugitive#head()
        return strlen(_) ? ' '._ : ''
    endif
    return ''
endfunction

function! LightLineFileformat()
    return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! LightLineFiletype()
    return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

function! LightLineFileencoding()
    return winwidth(0) > 70 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! LightLineMode()
    return winwidth(0) > 60 ? lightline#mode() : ''
endfunction

"Tabs
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>

nnoremap te :tabedit <CR>
nnoremap tc :tabclose <CR>

nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprevious<CR>
nnoremap <leader>bc :bdelete<CR>

" Go mappings
nnoremap <leader>gl :GoLint<CR>
nnoremap <leader>gml :GoMetaLinter<CR>
nnoremap <leader>gd :GoDoc<CR>
nnoremap <leader>ge :GoRename<CR>
nnoremap <leader>gr :GoRun<CR>

" More Go Hightlighting!
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1


nnoremap <Leader>s :%s/\<<C-r><C-w>\>/
nnoremap <Leader>& :Tab /&<cr>


augroup reload_vimrc
        autocmd!
            autocmd bufwritepost $MYVIMRC nested source $MYVIMRC
augroup END

let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<s-z>"

"let g:neosnippet#disable_runtime_snippets = {
"            \ '_' : 1,
"            \ }

"let g:neosnippet#snippets_directory='~/.vim/snippets/'

" Plugin key-mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)
"

"set foldmethod=indent
"set foldnestmax=1
"set nofoldenable
"set foldlevel=0
"
set pastetoggle=<F2>

" So it actually detects pgsql...
au BufRead,BufNewFile *.pgsql set filetype=pgsql

set spelllang=en_gb
" Latex
"au BufRead,BufNewFile *.tex set wrap
"au BufRead,BufNewFile *.tex set linebreak
"au BufRead,BufNewFile *.tex set tw=80
au BufRead,BufNewFile *.tex set spell

